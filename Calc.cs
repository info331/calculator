namespace Calculator
{
    public class Calc
    {
        public double Add(double a, double b)
        {
            return a + b;
        }

        public double ConvertFromEuros(double euros)
        {
            return euros * 10;
        }
    }
}