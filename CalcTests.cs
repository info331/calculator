﻿using NUnit.Framework;

namespace Calculator
{
    [TestFixture]
    public class CalcTests
    {
        [Test]
        public void AddNumbers()
        {
            var calc = new Calc();
            Assert.AreEqual(5, calc.Add(2, 3));
        }

        [Test]
        public void ConvertFromEuros()
        {
            var calc = new Calc();
            Assert.AreEqual(100, calc.ConvertFromEuros(10));
        }
    }
}
